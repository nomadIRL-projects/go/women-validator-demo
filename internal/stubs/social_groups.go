package stubs

import "WomenValidatorDemo/pkg/models"

var SocialGroups = map[string]int{
	"Подслушано в Зажопинске":                          models.SocialGroupTypeNews,
	"Отношения (Пошлое 18+)":                           models.SocialGroupTypePsychology,
	"ОБРАЗ ЖИЗНИ|психология,отношения,общество|цитаты": models.SocialGroupTypePsychology,
	"FOMINA":      models.SocialGroupTypeCelebrities,
	"Марк Бартон": models.SocialGroupTypePsychology,
	"Штукенция":   models.SocialGroupTypePsychology,
	"Слухи Ютуба": models.SocialGroupTypeOther,
	"Знакомства, общение - Любовь и отношения": models.SocialGroupTypePsychology,
	"Psychology|Психология":                    models.SocialGroupTypePsychology,
	"Сеть кинотеатров КАРО":                    models.SocialGroupTypeEntertainment,
	"Телеканал ТНТ":                            models.SocialGroupTypeEntertainment,
	"CHIP":                                     models.SocialGroupTypeOther,
	"КАРДО | Улица начинается здесь":           models.SocialGroupTypeOther,
	"ПУШКА": models.SocialGroupTypeOther,
	"Skillbox: образовательная платформа": models.SocialGroupTypeOther,
	"Умная мама":      models.SocialGroupTypeFamilyAndKids,
	"ДЕТИ В ФОКУСЕ":   models.SocialGroupTypeFamilyAndKids,
	"Полезные советы": models.SocialGroupTypeOther,
	"Илья Бусуркин — Психология добрых отношений": models.SocialGroupTypePsychology,
}
