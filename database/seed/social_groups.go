package seed

import (
	"WomenValidatorDemo/internal/stubs"
	"WomenValidatorDemo/pkg/models"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"sync"
)

type socialGroups struct {
	db    *sqlx.DB
	model *models.SocialGroup
}

func (s socialGroups) seed(db *sqlx.DB, wg *sync.WaitGroup) {
	defer wg.Done()

	s.db = db

	sgStubs := stubs.SocialGroups

	for name, sgType := range sgStubs {
		model := s.generateSocialGroup(name, sgType)

		s.insert(*model)
	}

	fmt.Println("Social groups seeded OK")
}

func (s socialGroups) generateSocialGroup(name string, groupType int) *models.SocialGroup {
	sg := models.SocialGroup{}

	sg.Name = name
	sg.Type = groupType

	return &sg
}

func (s socialGroups) insert(model models.SocialGroup) {
	q := fmt.Sprintf(
		"INSERT INTO %s (name, type) VALUES ($1,$2)",
		models.SocialGroupsTableName,
	)

	_, err := s.db.Exec(q, model.Name, model.Type)
	if err != nil {
		log.Fatal("Unable to insert social groups: ", err.Error())
	}
}
