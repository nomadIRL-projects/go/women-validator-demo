package seed

import (
	"WomenValidatorDemo/internal/helpers"
	"WomenValidatorDemo/internal/stubs"
	"WomenValidatorDemo/pkg/models"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"math/rand"
	"sync"
	"time"
)

var (
	minDiseaseAge    = 14   // Мин. возраст, при котором можно заболеть
	sickHumanRatio   = 0.13 // Часть людей в БД, которые хоть раз болели чем-то
	diseasesPerHuman = 10   // Макс. число болезней у одного человека
	healChance       = 75   // (%) Шанс на исцеление от одного заболевания
)

type humanDiseases struct {
	db    *sqlx.DB
	model models.Human
}

func (h *humanDiseases) seed(db *sqlx.DB, wg *sync.WaitGroup) {
	defer wg.Done()
	var diseaseWg sync.WaitGroup

	h.db = db

	sickHumans := h.selectSickHumans()

	for _, human := range *sickHumans {
		diseaseWg.Add(1)

		go h.spreadSickness(human, &diseaseWg)
	}

	diseaseWg.Wait()

	fmt.Println("Human diseases seeded OK")
}

func (h *humanDiseases) selectSickHumans() *[]models.Model {
	rows, err := h.db.Query(fmt.Sprintf(`
SELECT * FROM %s WHERE birth_date <= current_date - make_interval(years := $1)
AND ((death_date is null) OR (date(birth_date) + make_interval(years := $1) <= date(death_date)) )
AND random() < $2
`,
		models.HumansTableName),
		minDiseaseAge, sickHumanRatio)
	if err != nil {
		log.Fatal("Unable to find sick humans: ", err.Error())
	}

	defer rows.Close()

	sickHumans, err := h.model.ExtractAll(rows)
	if err != nil {
		log.Fatal("Failed to wrap possible sick humans: ", err.Error())
	}

	return &sickHumans
}

func (h *humanDiseases) spreadSickness(model models.Model, dwg *sync.WaitGroup) {
	defer dwg.Done()

	human := model.(models.Human)

	diseasesCount := rand.Intn(diseasesPerHuman)

	for i := 0; i <= diseasesCount; i++ {
		hd := h.generate(&human)
		h.insert(hd)
	}
}

func (h *humanDiseases) generate(human *models.Human) *models.HumanDisease {
	hd := models.HumanDisease{HumanId: human.Id}

	hd.Name = stubs.DiseaseNames[rand.Intn(len(stubs.DiseaseNames))]
	hd.Description = stubs.DiseaseDescriptions[rand.Intn(len(stubs.DiseaseDescriptions))]

	birthDate, _ := time.Parse(helpers.DateFormat, human.BirthDate)
	minStart := birthDate.AddDate(minDiseaseAge, 0, 0).Format(helpers.DateFormat)
	isHealed := rand.Intn(101) <= healChance
	_, diseaseStarted := helpers.GetRandomDateBetweenStrings(minStart, human.DeathDate)

	var diseaseEnded *string

	if isHealed {
		_, str := helpers.GetRandomDateBetweenStrings(diseaseStarted, human.DeathDate)
		diseaseEnded = &str
	}

	hd.DateStart = diseaseStarted
	hd.DateEnd = diseaseEnded

	return &hd
}

func (h *humanDiseases) insert(hd *models.HumanDisease) {
	q := fmt.Sprintf(
		"INSERT INTO %s (human_id, name, description, date_start, date_end) VALUES ($1,$2,$3,$4,$5)",
		models.HumanDiseasesTableName,
	)

	_, err := h.db.Exec(q, hd.HumanId, hd.Name, hd.Description, hd.DateStart, hd.DateEnd)
	if err != nil {
		log.Fatal("Unable to insert diseases: ", err.Error())
	}
}
