/**
  Создается промежуточная таблица с людьми, которые на данный момент старше $1 лет:
  $1 => мин. возраст вступления в отношения указанного типа
 */
-- name: 1step
SELECT id, name, gender, birth_date, death_date
INTO TEMP possible_initiators
FROM humans
WHERE date(birth_date) <= (current_date - make_interval(years := $1));

/**
  Создается выборка людей, которые не состоят в связях Родитель-ребенок,
  и где каждый человек старше $1 лет:
  > Прочие родственные связи на подобие брат-сестра не являются помехой при построении пар;
  > Люди могут иметь несколько отношений в момент времени;

  $1 => мин. возраст вступления в отношения указанного типа
  $2 => тип отношений
  $3 => процент распада отношений указанного типа
  $4 => насыщенность отношений указанного типа в базе; 0 - нет отношений, 1 - "каждый возможный с каждым возможным"
  $5 => должны ли пары быть разного пола
 */
-- name: 2step
INSERT INTO human_relationships
SELECT nextval('human_relationships_id_seq') AS id,
       human1,
       human2,
       type,
       date_start,
       (
           SELECT
                  CASE WHEN (date(max_end) = current_date AND random() > $3)
                      THEN null
                      ELSE date(date_start::timestamp + random() * (max_end::timestamp - date_start::timestamp))
                  END
        ) AS date_end
FROM (
    SELECT initiator_id AS human1,
           related_id AS human2,
           $2::int AS type,
           date(date(min_start)::timestamp + random() * (date(max_end)::timestamp - date(min_start)::timestamp)) AS date_start,
           max_end,
           min_start
    FROM (
        SELECT initiators.id AS initiator_id,
               related.id AS related_id,
               greatest(date(date(initiators.birth_date) + make_interval(years := $1)), date(date(related.birth_date) + make_interval(years := $1))) AS min_start,
               least(date(initiators.death_date), date(related.death_date), current_date) AS max_end
        FROM human_relationships
            LEFT JOIN possible_initiators AS initiators ON initiators.id = human_relationships.human1
            JOIN humans AS related ON related.id != human_relationships.human2
        WHERE type = 1 -- Тип отношений: Родитель-ребенок
          AND random() > $4
          AND initiators.id != related.id
          AND (date(related.birth_date) + make_interval(years := $1)) <= current_date
          AND CASE WHEN ($5) THEN related.gender != initiators.gender ELSE initiators.id > 0 END
        ) AS basic
    ) AS aggregated
WHERE (date(min_start) <= date(max_end));

/**
  Удалить параллельные отношения указанного типа:
  $1 => тип отношений, которые должны идти последовательно (в момент времени их не может быть несколько с разными людьми)
  */
-- name: 3step
DELETE FROM human_relationships
WHERE type = $1
  AND id NOT IN (
    SELECT id
    FROM (
             SELECT id, human1, date_start, lag(date_end) OVER (PARTITION BY human1 ORDER BY date(date_end)) AS previous_end
             FROM (SELECT DISTINCT ON (human2, date_end) *
                   FROM (
                            SELECT DISTINCT ON (human1, date_end) id,
                                                                  human1,
                                                                  human2,
                                                                  date_start,
                                                                  date_end
                            FROM human_relationships
                            WHERE type = $1
                        ) AS uh1
                  ) AS uh2
        ) AS aggregated
    WHERE date(date_start) >= date(previous_end) OR previous_end IS null
)