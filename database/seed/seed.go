package seed

import (
	App "WomenValidatorDemo/pkg/core"
	"github.com/jmoiron/sqlx"
	"math/rand"
	"sync"
	"time"
)

type Seeder interface {
	seed(db *sqlx.DB, wg *sync.WaitGroup)
}

func Boot() {
	DB := *App.Boot(false).DB

	seeders := []Seeder{
		new(humanRelationships),
		new(humanDiseases),
		new(socialGroups),
		//new(socialGroupUsers),
		//new(socialGroupUserComments),
	}

	var wg sync.WaitGroup

	wg.Add(len(seeders))

	rand.Seed(time.Now().UnixNano())

	// Сперва нужно сгенерировать людей; вокруг них строится все остальное
	new(humans).seed(&DB, nil)

	for _, seeder := range seeders {
		go seeder.seed(&DB, &wg)
	}

	wg.Wait()
}
