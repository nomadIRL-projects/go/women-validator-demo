package seed

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"sync"
)

type socialGroupUserComments struct{}

func (s socialGroupUserComments) seed(db *sqlx.DB, wg *sync.WaitGroup) {
	defer wg.Done()

	fmt.Println("Social group user comments seeded OK")
}

func _() {

}
