/**
  Для миграции используется утилита migrate:
  > go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

  Откатываем миграцию командой:
  $GOPATH/bin/migrate -path ./database/migrations -database 'postgres://postgres:qwerty@localhost:5436/postgres?sslmode=disable' down

  (см. пароль в .env)
 */

DROP TABLE social_group_user_comments;
DROP TABLE social_group_users;
DROP TABLE social_groups;
DROP TABLE human_diseases;
DROP TABLE human_relationships;
DROP TABLE humans;