/**
  Для миграции используется утилита migrate:
  > go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

  Мигрируем командой:
  $GOPATH/bin/migrate -path ./database/migrations -database 'postgres://postgres:qwerty@localhost:5436/postgres?sslmode=disable' up

  (см. пароль в .env)
 */

CREATE TABLE humans
(
    id serial not null unique,
    name varchar(100) not null,
    surname varchar(100) not null,
    middle_name varchar (100),
    gender boolean not null , /* мужской пол - true */
    birth_date date not null,
    death_date date
);

CREATE TABLE human_relationships (
    id serial not null unique,
    human1 int references humans(id) on delete cascade not null,
    human2 int references humans(id) on delete cascade not null,
    type smallint not null,
    date_start date not null,
    date_end date
);

CREATE TABLE human_diseases (
    id serial not null unique,
    human_id int references humans(id) on delete cascade not null,
    name varchar(1000) not null, /* Для упрощения структуры сделано просто как текст... */
    description text,
    date_start date not null,
    date_end date
);

CREATE TABLE social_groups (
    id serial not null unique,
    name varchar(255) unique not null,
    description text,
    type smallint not null
);

CREATE TABLE social_group_users (
    id serial not null unique,
    group_id int references social_groups(id) on delete cascade not null,
    human_id int references humans(id) on delete cascade not null
);

CREATE TABLE social_group_user_comments (
    id serial not null unique,
    group_id int references social_groups(id) on delete cascade not null,
    human_id int references humans(id) on delete cascade not null,
    comment text not null
);