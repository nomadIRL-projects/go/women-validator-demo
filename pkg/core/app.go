package App

import (
	db "WomenValidatorDemo/config"
	"WomenValidatorDemo/pkg/core/Server"
	appRoutes "WomenValidatorDemo/pkg/routes"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
)

// App Объект готового к работе приложения
type App struct {
	DB *sqlx.DB
}

func Boot(bootServer bool) *App {
	app := new(App)

	setEnv()
	go runServer(bootServer)

	app.DB = ConnectToDb()

	return app
}

func setEnv() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf(".env file is not specified")
	}
}

func runServer(boot bool) {
	if !boot {
		return
	}

	routes := appRoutes.Routes()
	server := new(Server.Server)

	server.Run(routes)
}

func ConnectToDb() *sqlx.DB {
	cfg := db.GetActualConfig()

	DB, err := sqlx.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
			cfg.Host, cfg.Port, cfg.Username, cfg.DBName, cfg.Password, cfg.SSLMode,
		),
	)

	if err != nil {
		log.Fatal("Unable to connect to Database " + err.Error())
	}

	err = DB.Ping()

	if err != nil {
		log.Fatal(fmt.Sprintf("DB Ping error: %s", err.Error()))
	}

	return DB
}
