package models

import "database/sql"

// Human из таблицы `humans`
type Human struct {
	Id         uint64  // ID человека
	Name       string  // Имя
	Surname    string  // Фамилия
	MiddleName string  // Отчество
	Gender     bool    // Пол
	BirthDate  string  // Дата рождения
	DeathDate  *string // Дата смерти
}

func (m Human) ExtractAll(rows *sql.Rows) ([]Model, error) {
	var humans []Model

	for rows.Next() {
		h := Human{}

		err := rows.Scan(&h.Id, &h.Name, &h.Surname, &h.MiddleName, &h.Gender, &h.BirthDate, &h.DeathDate)
		if err != nil {
			return nil, err
		}

		humans = append(humans, h)
	}

	return humans, nil
}
