package models

import (
	"database/sql"
)

type HumanDisease struct {
	Id          uint64
	HumanId     uint64
	Name        string
	Description string
	DateStart   string
	DateEnd     *string
}

func (m HumanDisease) ExtractAll(rows *sql.Rows) ([]Model, error) {
	var humanDiseases []Model

	for rows.Next() {
		hd := HumanDisease{}

		err := rows.Scan(&hd.Id, &hd.HumanId, &hd.Name, &hd.Description, &hd.DateStart, &hd.DateEnd)
		if err != nil {
			return nil, err
		}

		humanDiseases = append(humanDiseases, hd)
	}

	return humanDiseases, nil
}
