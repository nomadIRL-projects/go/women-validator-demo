package routes

import (
	rest "WomenValidatorDemo/pkg/handlers/api"
	"github.com/gin-gonic/gin"
)

// Подгрузить rest-api роуты
func collectApiRoutes(router *gin.Engine) {
	api := router.Group("/v1/api")

	api.GET("/search/:name", rest.SearchResults)
}
