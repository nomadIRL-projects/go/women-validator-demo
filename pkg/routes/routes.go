package routes

import "github.com/gin-gonic/gin"

func Routes() *gin.Engine {
	router := gin.New()

	collectRoutes(router)

	return router
}

// Подгрузить все возможные роуты приложения
func collectRoutes(router *gin.Engine) {
	collectWebRoutes(router)
	collectApiRoutes(router)
}
